<?php

require "Helper.php";

class Yatzy
{
    public function __construct($d1, $d2, $d3, $d4, $_5)
    {
        $this->dice = array_fill(0, 6, 0);
        $this->dice[0] = $d1;
        $this->dice[1] = $d2;
        $this->dice[2] = $d3;
        $this->dice[3] = $d4;
        $this->dice[4] = $_5;
    }

    public static function chance($d1, $d2, $d3, $d4, $d5)
    {
        $total = 0;
        $total += $d1;
        $total += $d2;
        $total += $d3;
        $total += $d4;
        $total += $d5;
        return $total;
    }

    public static function yatzyScore($dice)
    {
        $counts = array_fill(0, count($dice) + 1, 0);
        foreach ($dice as $die) {
            $counts[$die - 1] += 1;
        }
        foreach (range(0, count($counts) - 1) as $i) {
            if ($counts[$i] == 5)
                return 50;
        }
        return 0;
    }

    public static function ones($d1, $d2, $d3, $d4, $d5)
    {
        return Helper::compareDiceValue([
            $d1, $d2, $d3, $d4, $d5
        ], 1);
    }

    public static function twos($d1, $d2, $d3, $d4, $d5)
    {
        return Helper::compareDiceValue([
            $d1, $d2, $d3, $d4, $d5
        ], 2);
    }

    public static function threes($d1, $d2, $d3, $d4, $d5)
    {
        return Helper::compareDiceValue([
            $d1, $d2, $d3, $d4, $d5
        ], 3);
    }   

    public function fours()
    {
        return Helper::compareDiceValue($this->dice, 4);
    }

    public function fives()
    {
        return Helper::compareDiceValue($this->dice, 5);
    }

    public function sixes()
    {
        return Helper::compareDiceValue($this->dice, 6);
    }

    public static function scorePair($d1, $d2, $d3, $d4, $d5)
    {
        $counts = Helper::countDiceOccurance([$d1, $d2, $d3, $d4, $d5]);
        for ($at = 0; $at != 6; $at++)
            if ($counts[6 - $at - 1] == 2)
                return (6 - $at) * 2;
        return 0;
    }

    public static function twoPair($d1, $d2, $d3, $d4, $d5)
    {
        $counts = Helper::countDiceOccurance([$d1, $d2, $d3, $d4, $d5]);
        $n = 0;
        $score = 0;
        for ($i = 0; $i != 6; $i++)
            if ($counts[6 - $i - 1] >= 2) {
                $n = $n + 1;
                $score += (6 - $i);
            }

        if ($n == 2)
            return $score * 2;
        else
            return 0;
    }

    public static function threeOfKind($d1, $d2, $d3, $d4, $d5)
    {
        $counts = Helper::countDiceOccurance([$d1, $d2, $d3, $d4, $d5]);
        for ($i = 0; $i != 6; $i++)
            if ($counts[$i] >= 3)
                return ($i + 1) * 3;
        return 0;
    }

    public static function smallStraight($d1, $d2, $d3, $d4, $d5)
    {
        $tallies = Helper::countDiceOccurance([$d1, $d2, $d3, $d4, $d5]);
        if ($tallies[0] == 1 &&
            $tallies[1] == 1 &&
            $tallies[2] == 1 &&
            $tallies[3] == 1 &&
            $tallies[4] == 1)
            return 15;
        return 0;
    }

    public static function largeStraight($d1, $d2, $d3, $d4, $d5)
    {
        $tallies = Helper::countDiceOccurance([$d1, $d2, $d3, $d4, $d5]);
        if ($tallies[1] == 1 &&
            $tallies[2] == 1 &&
            $tallies[3] == 1 &&
            $tallies[4] == 1 &&
            $tallies[5] == 1)
            return 20;
        return 0;
    }

    public static function fullHouse($d1, $d2, $d3, $d4, $d5)
    {
        $tallies = [];
        $_2 = false;
        $i = 0;
        $_2_at = 0;
        $_3 = False;
        $_3_at = 0;

        $tallies = Helper::countDiceOccurance([$d1, $d2, $d3, $d4, $d5]);

        foreach (range(0, 5) as $i) {
            if ($tallies[$i] == 2) {
                $_2 = True;
                $_2_at = $i + 1;
            }

            if ($tallies[$i] == 3) {
                $_3 = True;
                $_3_at = $i + 1;
            }
        }

        if ($_2 && $_3)
            return $_2_at * 2 + $_3_at * 3;
        else
            return 0;
    }
}
