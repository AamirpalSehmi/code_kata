<?php

class Helper
{
    function compareDiceValue($dices, $identifire)
    {
        $dice_sum = array_count_values($dices);
        return ($dice_sum[$identifire]??0)*$identifire;
    }


    function countDiceOccurance($dices)
    {
        $counts = array_fill(0, 6, 0);
        $counts[$dices[0] - 1] += 1;
        $counts[$dices[1] - 1] += 1;
        $counts[$dices[2] - 1] += 1;
        $counts[$dices[3] - 1] += 1;
        $counts[$dices[4] - 1] += 1;
        return $counts;
    }
}